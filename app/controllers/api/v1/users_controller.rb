module Api
  module V1
    class UsersController < ApplicationController
      # GET /users with optional sorting
      def index
        if params[:sort_by]
          field, sort_option = params[:sort_by].split('_')
          field = 'year_salary' if field == 'salary'

          if field.match(/lastname|year_salary/) && sort_option.match(/asc|desc/)
            return render json: User.order(field => sort_option)
          end
        end
        render json: User.all
      end

      # POST /users
      def create
        user = User.new(user_params)

        if user.save
          render json: user, status: 201
        else
          render json: user.errors, status: 422
        end
      end

      # GET /users/:id
      def show
        @user = User.find(params[:id])
        render json: @user
      rescue ActiveRecord::RecordNotFound
        render json: { error: "User with id #{params[:id]} doesn't exists" }, status: 404
      end

      # PUT /users/:id
      def update
        @user = User.find(params[:id])

        if @user.update(user_params)
          render json: @user
        else
          render json: @user.errors, status: 422
        end
      rescue ActiveRecord::RecordNotFound
        render json: { error: "User with id #{params[:id]} doesn't exists" }, status: 404
      end

      # DELETE /users/:id
      def destroy
        User.find(params[:id]).destroy
        head 204
      rescue ActiveRecord::RecordNotFound
        render json: { error: "User with id #{params[:id]} doesn't exists" }, status: 404
      end

      private

      def user_params
        params.require(:user).permit(:firstname, :lastname, :year_salary)
      end
    end
  end
end
