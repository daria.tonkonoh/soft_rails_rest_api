import Users from './Users'

function App() {
  return (
    <div className="container">
      <header>
        <h1>Users App</h1>
      </header>

      <Users />
    </div>
  )
}

export default App
