import axios from 'axios'

const USERS_API = '/api/v1/users'

export async function getUsers(sort_field = null) {
  try {
    const url = sort_field ? `${USERS_API}?sort_by=${sort_field}` : USERS_API
    const response = await axios(url)
    return response.data
  } catch (e) {
    console.log(e)
  }
}

export async function createUser(fields) {
  try {
    const response = await axios.post(USERS_API, fields)
    return response.data
  } catch (e) {
    console.log(e)
  }
}

export async function updateUser(id, fields) {
  try {
    const response = await axios.put(`${USERS_API}/${id}`, fields)
    return response.data
  } catch (e) {
    console.log(e)
  }
}

export async function deleteUser(id) {
  try {
    await axios.delete(`${USERS_API}/${id}`)
  } catch (e) {
    console.log(e)
  }
}
