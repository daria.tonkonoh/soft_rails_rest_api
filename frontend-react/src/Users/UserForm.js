import { useEffect, useState } from 'react'

function UserForm({ userValues, createUser, updateUser }) {
  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [year_salary, setYearSalary] = useState('')
  const [buttonText, setButtonText] = useState('Create User')
  const [isEditingUser, setIsEditingUser] = useState(false)

  useEffect(() => {
    if (Object.keys(userValues).length) {
      const { firstname, lastname, year_salary } = userValues
      setIsEditingUser(true)
      setFirstname(firstname)
      setLastname(lastname)
      setYearSalary(year_salary)
      setButtonText('Update User')
    }
  }, [userValues])

  const cleanForm = () => {
    setIsEditingUser(false)
    setFirstname('')
    setLastname('')
    setYearSalary('')
    setButtonText('Create User')
  }

  const onFormSubmit = (e) => {
    e.preventDefault()
    if (isEditingUser) {
      updateUser(userValues.id, { firstname, lastname, year_salary })
      cleanForm()
    } else {
      createUser({ firstname, lastname, year_salary })
      cleanForm()
    }
  }

  return (
    <div>
      <form onSubmit={onFormSubmit} className="mb-3 col-6">
        <div className="mb-2">
          <label htmlFor="firstname" className="form-label">
            First Name
          </label>
          <input
            type="text"
            className="form-control"
            id="firstname"
            value={firstname}
            onChange={(e) => setFirstname(e.target.value)}
            required
          />
        </div>
        <div className="mb-2">
          <label htmlFor="lastname" className="form-label">
            Last Name
          </label>
          <input
            type="text"
            className="form-control"
            id="lastname"
            value={lastname}
            onChange={(e) => setLastname(e.target.value)}
            required
          />
        </div>
        <div className="mb-2">
          <label htmlFor="year_salary" className="form-label">
            Year Salary
          </label>
          <input
            type="number"
            min="0"
            className="form-control"
            id="year_salary"
            value={year_salary}
            onChange={(e) => setYearSalary(+e.target.value)}
            required
          />
        </div>
        <button type="submit" className="btn btn-primary me-2">
          {buttonText}
        </button>

        <button type="button" className="btn btn-light" onClick={cleanForm}>
          Cancel
        </button>
      </form>
    </div>
  )
}

export default UserForm
