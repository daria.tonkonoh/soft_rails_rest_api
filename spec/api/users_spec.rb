require 'rails_helper'

describe 'Users API', type: :request do
  describe 'GET /users' do
    it 'returns all users' do
      FactoryBot.create(:user, firstname: 'foo', lastname: 'bar', year_salary: 50000)
      get '/api/v1/users'
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)).not_to be_empty
      expect(JSON.parse(response.body).size).to eq(1)
    end
  end

  describe 'POST /users' do
    it 'creates a new user' do
      expect {
        post '/api/v1/users', params: {
          user: { firstname: 'foo', lastname: 'bar', year_salary: 50000 }
        }
      }.to change { User.count }.from(0).to(1)
      expect(response).to have_http_status(201)
    end
  end

  describe 'DELETE /users/:id' do
    let!(:user) { FactoryBot.create(:user, firstname: 'foo', lastname: 'bar', year_salary: 50000) }

    it 'deletes a user' do
      expect {
        delete "/api/v1/users/#{user.id}"
      }.to change { User.count }.from(1).to(0)
      expect(response).to have_http_status(204)
    end
  end
end
