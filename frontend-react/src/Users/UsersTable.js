import { useState } from 'react'

const sortingOptions = [
  { value: 'default', label: 'Default' },
  { value: 'salary_asc', label: 'Salary asc' },
  { value: 'salary_desc', label: 'Salary desc' },
  { value: 'lastname_desc', label: 'Last name asc' },
  { value: 'lastname_asc', label: 'Last name desc' },
]

function UsersTable({ users, sortUsers, selectedUser, deleteUser }) {
  const [checkedValue, setCheckedValue] = useState('default')

  const onSortingChange = (e) => {
    sortUsers(e.target.value)
  }

  return (
    <>
      <div onChange={onSortingChange}>
        <h6>Sort by:</h6>

        {sortingOptions.map((sorting, i) => (
          <div key={i} className="form-check form-check-inline">
            <label className="form-check-label">
              <input
                className="form-check-input"
                type="radio"
                name="sort"
                value={sorting.value}
                checked={checkedValue === sorting.value}
                onChange={(e) => setCheckedValue(e.currentTarget.value)}
              />
              {sorting.label}
            </label>
          </div>
        ))}
      </div>

      <table className="table mt-3">
        <thead>
          <tr>
            <th>id</th>
            <th>first name</th>
            <th>last name</th>
            <th>year salary</th>
            <th>actions</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => (
            <tr key={user.id}>
              <td>{user.id}</td>
              <td>{user.firstname}</td>
              <td>{user.lastname}</td>
              <td>{user.year_salary}</td>
              <td>
                <div className="d-grid gap-2 d-md-block">
                  <button
                    className="btn btn-outline-secondary btn-sm me-2"
                    type="button"
                    onClick={() => selectedUser(user)}
                  >
                    Edit
                  </button>
                  <button
                    className="btn btn-outline-danger btn-sm"
                    type="button"
                    onClick={() => deleteUser(user.id)}
                  >
                    Delete
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

export default UsersTable
