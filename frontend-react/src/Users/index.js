import { useEffect, useState } from 'react'
import { getUsers, createUser, updateUser, deleteUser } from '../services/users'
import UserForm from './UserForm'
import UsersTable from './UsersTable'

function Users() {
  const [users, setUsers] = useState([])
  const [userValues, setUserValues] = useState({})

  const updateUsers = async (sort_field = null) => {
    const users = await getUsers(sort_field)
    setUsers(users)
  }

  useEffect(() => {
    if (!users.length) updateUsers()
  }, [users])

  const handleCreateUser = async (fields) => {
    const newUser = await createUser(fields)
    setUsers([...users, newUser])
  }

  const handleUpdateUser = async (id, fields) => {
    const updatedUser = await updateUser(id, fields)

    const foundIndex = users.findIndex((u) => u.id === id)
    let updatedUsers = [...users]
    updatedUsers[foundIndex] = { ...updatedUser }
    setUsers(updatedUsers)
  }

  const handleDeleteUser = async (id) => {
    await deleteUser(id)

    const foundIndex = users.findIndex((u) => u.id === id)
    let updatedUsers = [...users]
    updatedUsers.splice(foundIndex, 1)
    setUsers(updatedUsers)
  }

  return (
    <div>
      <UserForm
        userValues={userValues}
        createUser={handleCreateUser}
        updateUser={handleUpdateUser}
      />

      {users && (
        <UsersTable
          users={users}
          sortUsers={updateUsers}
          selectedUser={setUserValues}
          deleteUser={handleDeleteUser}
        />
      )}
    </div>
  )
}

export default Users
