require 'swagger_helper'

# These tests use rswag DSL for generating swagger.yaml
RSpec.describe 'Users API', type: :request do

  path '/api/v1/users' do

    get('List of users') do
      produces 'application/json'
      response(200, 'JSON array of users') do
        schema type: 'array',
               items: { '$ref' => '#/components/schemas/user' }
        run_test!
      end
    end

    post('Create user') do
      consumes 'application/json'
      produces 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          firstname: { type: :string },
          lastname: { type: :string },
          year_salary: { type: :integer }
        },
        required: %w[firstname lastname year_salary]
      }

      response(201, 'Return created user') do
        schema '$ref' => '#/components/schemas/user'
        let(:user) { { firstname: 'foo', lastname: 'bar', year_salary: 50000 } }
        run_test!
      end

      response '422', 'Invalid request data' do
        let(:user) { { firstname: 'foo' } }
        run_test!
      end
    end
  end

  path '/api/v1/users/{id}' do
    parameter name: 'id', in: :path, type: :integer, description: 'user id'

    get('Get user by id') do
      produces 'application/json'

      response(200, 'Found user') do
        schema '$ref' => '#/components/schemas/user'
        let(:id) { User.create(firstname: 'foo', lastname: 'bar', year_salary: 50000).id }
        run_test!
      end

      response '404', 'User not found' do
        let(:id) { 9999999 }
        run_test!
      end
    end

    # patch('update user') do
    #   response(200, 'successful') do
    #     let(:id) { '123' }
    #
    #     after do |example|
    #       example.metadata[:response][:content] = {
    #         'application/json' => {
    #           example: JSON.parse(response.body, symbolize_names: true)
    #         }
    #       }
    #     end
    #     run_test!
    #   end
    # end

    put('Update user') do
      consumes 'application/json'
      produces 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          firstname: { type: :string },
          lastname: { type: :string },
          year_salary: { type: :integer }
        }
      }
      let(:user) { User.create(firstname: 'foo', lastname: 'bar', year_salary: 1) }

      response(200, 'Updated user') do
        let(:id) { user.id }
        schema '$ref' => '#/components/schemas/user'
        run_test!
      end

      response '404', 'User not found' do
        let(:id) { 9999999 }
        run_test!
      end
    end

    delete('Delete user') do
      response(204, 'User was deleted successfully') do
        let(:id) { User.create(firstname: 'foo', lastname: 'bar', year_salary: 50000).id }
        run_test!
      end

      response '404', 'User not found' do
        let(:id) { 9999999 }
        run_test!
      end
    end
  end
end
