class User < ApplicationRecord
  validates :firstname, :lastname, :year_salary, presence: true
end
